# Table of content
[[_TOC_]]

# Description
## Purpose
The purpose of this device is to store variables as Tango attributes.

## State
The state is ON at the end of the initialization phase. It can be set later during the instance lifetime thru the command setState.

## Status
At the end of the init phase, the status displays the number of created attributes.

# Properties
## Definition
The only property is `AttributesList`. Each line represents an attribute to create. The parameters are separated by the character `';'`.
There is two possible configuration for an attribute:
<ol>
	<li>
		By the name and full type:</li>
		<ul>
			<li>Attribute name</li>
			<li>Attribute type as a string representing the type and format. [See the dedicated chapter](#attribute-full-type)</li>
		</ul>
	</li>
	<li>
		By the name and type and format. It allows to use more types.
		<ul>
			<li>Attribute name</li>
			<li>
				Attribute type. It can by set by a number or a string representing it. [See the dedicated chapter](#attribute-type-without-format)</li>
			<li>
				Attribute format. The possible values are:
				<ul>
					<li>SCALAR</li>
					<li>SPECTRUM</li>
					<li>IMAGE</li>
				</ul>
			</li>
		</ul>
	</li>
</ol>

## Attribute full type
| Type       |Matching tango type |
|:-----------|:-------------------|
| int        | DevLong            |
| long       | DevLong64          |
| double     | DevDouble          |
| float      | DevFloat           |
| boolean    | DevBoolean         |
| byte       | DevUChar           |
| short      | DevShort           |
| string     | DevString          |
| devstate   | DeviceState        |
| state      | DeviceState        |
| devencoded | DevEncoded         |

Adding `[]` turns the attribute to a spectrum, adding `[][]` turns it to an image. For example `double[]` will be a spectrum of DevDouble and `short\[][]` will be an image of short. Otherwise, the attribute will be a scalar.

## Attribute type without format
It can be set as a number or a string. The string value matches the tango type.

| Number |Matching tango type |
|:------:|:-------------------|
| 1      | DevBoolean         |
| 2      | DevShort           |
| 3      | DevLong            |
| 4      | DevFloat           |
| 5      | DevDouble          |
| 6      | DevUShort          |
| 7      | DevULong           |
| 8      | DevString          |
| 23     | DevLong64          |
| 24     | DevULong64         |
| 22     | DevUChar           |
| 19     | DeviceState        |
| 28     | DevEncoded         |
| 29     | DevEnum            |


## Examples
### Correct entries
| Attribute definition | Resulting tango type     |
|:--------------------:|:-------------------------|
| attr1;1;SPECTRUM     | A spectrum of DevBoolean |
| attr2;string         | A DevString scalar       |
| attr3;double[]       | A spectrum of DevDouble  |

### Wrong entries
| Attribute definition    | Error                                                                                                                 |
|:-----------------------:|:----------------------------------------------------------------------------------------------------------------------|
| attr3;double[];SPECTRUM | This string definition doesn't require to specify the format. Use DevDouble, 5 or remove the format.                  |
| attr3;double;SPECTRUM   | This string definition doesn't require to specify the format. Use DevDouble, 5 or remove the format and use double[]. |
| attr4;5                 | If a number is used, the format must be specified.                                                                    |

# Attributes
All attributes are dynamically created from the property `AttributesList` in READ/WRITE mode and at operator level.

# Initial values
According to the type, the initial value will be zero for numerical values, false for booleans or empty for strings, spectrum and image.

# Commands
| Name       |Parameters                       | Description                                                                              |
|:-----------|:--------------------------------|:-----------------------------------------------------------------------------------------|
| Init       | None                            | Reinit the device. The properties will be parsed again and attributes will be recreated. |
| state      | None                            | Returns the current state                                                                |
| status     | None                            | Returns the current status                                                               |
| setState   | A string representing the state | Sets the device state to the given value.<br/>Possible values are: <ul><li>ON</li><li>OFF</li><li>CLOSE</li><li>OPEN</li><li>INSERT</li><li>EXTRACT</li><li>MOVING</li><li>STANDBY</li><li>FAULT</li><li>INIT</li><li>RUNNING</li><li>ALARM</li><li>DISABLE</li><li>UNKNOWN</li></ul>.In upper case only. |


package fr.soleil.tango.server.publisher;

import java.util.Arrays;

import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.dynamic.attribute.PublisherAttribute;

import fr.esrf.Tango.DevFailed;

@Device(transactionType = TransactionType.NONE)
public final class Publisher {

    @Status
    private String status;

    @State
    private DeviceState state;

    @DynamicManagement
    private DynamicManager dynMngt;
    @DeviceProperty
    private String[] attributesList;

    // commandsList was on publisher for test, must be move to another device
    // @DeviceProperty
    // String[] commandsList;

    @Init
    public void init() throws ClassNotFoundException, DevFailed {
        for (final String attribute : attributesList) {
            final String[] config = attribute.split(";");
            dynMngt.addAttribute(new PublisherAttribute(config));
        }
        // for (final String command : commandsList) {
        // final String[] config = command.split(";");
        // dynMngt.addCommand(new DummyDynamicCommand(config));
        // }
        status = dynMngt.getDynamicAttributes().size() + " attributes created";
        state = DeviceState.ON;
    }

    @Delete
    public void delete() throws DevFailed {
        dynMngt.clearAll();
    }

    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, Publisher.class);
    }

    public DeviceState getState() {
        return state;
    }

    @Command(inTypeDesc = "The state as a string (ON, OFF, RUNNING...)")
    public void setState(final String state) {
        this.state = DeviceState.valueOf(state);
    }

    public void setState(final DeviceState state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public void setDynMngt(final DynamicManager dynMngt) {
        this.dynMngt = dynMngt;
    }

    public void setAttributesList(final String[] attributesList) {
        this.attributesList = Arrays.copyOf(attributesList, attributesList.length);
    }
}

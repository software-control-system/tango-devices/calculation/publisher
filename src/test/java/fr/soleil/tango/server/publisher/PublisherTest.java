package fr.soleil.tango.server.publisher;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.tango.server.ServerManager;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;

public class PublisherTest {
    private static DeviceProxy thisDevice;
    private static final String noDbDeviceName = "1/1/1";
    private static final String noDbGiopPort = "12354";
    private static final String noDbInstanceName = "1";
    public static final String deviceName = "tango://localhost:" + noDbGiopPort + "/" + noDbDeviceName + "#dbase=no";

    @BeforeClass
    public static void setUpBeforeClass() throws DevFailed {
        try {
            startNoDb(Publisher.class);
            thisDevice = new DeviceProxy(deviceName);
            final DevState state = thisDevice.state();
            if (state.equals(DevState.FAULT)) {
                throw DevFailedUtils.newDevFailed(thisDevice.status());
            }
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    private static void startNoDb(final Class<?> deviceClass) throws DevFailed {
        System.setProperty("OAPort", noDbGiopPort);
        ServerManager.getInstance().addClass(deviceClass.getCanonicalName(), deviceClass);
        final String fileProps = Publisher.class.getResource("/deviceProperties").getPath();
        ServerManager.getInstance().start(
                new String[] { noDbInstanceName, "-nodb", "-dlist", noDbDeviceName, "-file=" + fileProps },
                "Publisher");
        // test connection
        new DeviceProxy(deviceName).status();

    }

    @Test
    public void testWriteReadAll() throws DevFailed {
        try {
            final String[] attrList = thisDevice.get_attribute_list();
            final int expected = 1;
            for (final String attributeName : attrList) {
                if (!attributeName.startsWith("exception") && !attributeName.equalsIgnoreCase("State")
                        && !attributeName.equalsIgnoreCase("Status") && !attributeName.equalsIgnoreCase("version")) {
                    System.out.println(attributeName);
                    final TangoAttribute attribute = new TangoAttribute(deviceName + "/" + attributeName);

                    if (attribute.isScalar()) {
                        attribute.write(expected);
                        final int actual = attribute.read(Integer.class);
                        assertThat(actual, equalTo(expected));

                    } else if (attribute.isSpectrum()) {
                        attribute.write(new Integer[] { expected });
                        attribute.read();
                        final Integer[] actual = attribute.extractSpecOrImage(Integer.class);
                        assertThat(actual[0], equalTo(expected));
                    } else {
                        attribute.write(new Integer[][] { { expected } });
                        attribute.read();
                        final Integer[] actual = attribute.extractSpecOrImage(Integer.class);
                        assertThat(actual[0], equalTo(expected));
                    }
                }
            }
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void timestamps() throws DevFailed {
        final TangoAttribute attribute = new TangoAttribute(deviceName + "/scalarShort");
        attribute.read();
        final long time1 = attribute.getTimestamp();
        System.out.println("time 1 " + time1);
        attribute.read();
        final long time2 = attribute.getTimestamp();
        System.out.println("time 2 " + time2);
        assertThat(time1, not(time2));
    }

    @Test(expected = DevFailed.class)
    public void testReadExceptionAttribute() throws DevFailed {
        final TangoAttribute attribute = new TangoAttribute(deviceName + "/exception");
        System.out.println(attribute.extract());
    }

    @AfterClass
    public static void after() throws DevFailed {
        // try {
        // Thread.sleep(1000000);
        // } catch (final InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        ServerManager.getInstance().stop();
    }
}
